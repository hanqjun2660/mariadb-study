# mariadb study

## Tables Info

## Getting started
MariaDB에 익숙해지도록 샘플 문제를 작성하여 해당 문제에 대한 답변을 문서에 기록한다.

> 사원 테이블의 모든 레코드를 조회하시오.
```
SELECT * FROM EMPLOYEES;
```

> 사원명과 입사일을 조회하시오.
```
SELECT
       first_name,
       last_name,
       hire_date
  FROM EMPLOYEES;
```

> 사원번호와 이름을 조회하시오.
```
SELECT
       first_name,
       last_name,
       emp_no
  FROM EMPLOYEES;
```

> 총 사원수를 구하시오.
```
SELECT
       COUNT(*)
  FROM employees;
```
> 부서번호가 d009인 사원을 조회하시오.
```
SELECT
       *
  FROM dept_emp a 
  JOIN employees b ON a.emp_no = b.emp_no
 WHERE a.dept_no = 'd009';
```

